// const fileUtils = require("../problem1"); // Replace './fileUtils' with the correct path to your module.

// Usage

const Mymodule = require('../problem1');
const createJSONUsingFS = Mymodule.createJSONUsingFS;
const createJSONUsingPromise = Mymodule.createJSONUsingPromise;
const createJSONUsingAsync = Mymodule.createJSONUsingAsync;

const obj = { Name: 'Anup', Profession: 'SDE', Age: '25' };
var details = JSON.stringify(obj, null, 2);

createJSONUsingFS(details);
createJSONUsingPromise(details);
createJSONUsingAsync(details);
