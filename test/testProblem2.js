const problem2Module = require('../problem2');
const readLipsum = problem2Module.readLipsum;
const makeUpperCase = problem2Module.makeUpperCase;
const readUpperMakeLower = problem2Module.readUpperMakeLower;
const createSentencesInFiles = problem2Module.createSentencesInFiles;
const readNewfilesAndSort = problem2Module.readNewfilesAndSort;
const deleteAllFiles = problem2Module.deleteAllFiles;

async function main() {
  await readLipsum();
  await makeUpperCase();
  await readUpperMakeLower();
  await createSentencesInFiles();
  await readNewfilesAndSort();
  await deleteAllFiles();
}
main();
