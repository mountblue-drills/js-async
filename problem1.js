const fs = require('fs');

//-----------part1-----------

// creating Details.JSON
function createJSONUsingFS(details) {
  fs.mkdir('./test/random', (err) => {
    if (!err) {
      console.log('New directory created.');
      for (let name = 1; name < 4; name++) {
        const filename = `./test/random/file${name}.json`;
        fs.writeFile(filename, details, function (err) {
          if (err) {
            console.log('error', err);
          } else {
            console.log(`file${name} created successfully.`);
          }
        });
      }
    } else {
      console.log('Error', err);
    }
  });

  // deleting Details.JSON
  for (let name = 1; name < 4; name++) {
    const fileName = `./test/random/file${name}.json`;
    setTimeout(() => {
      fs.unlink(fileName, (err) => {
        if (err) {
          console.log(`Error deleting file${name}:`, err);
        } else {
          console.log(`file${name} deleted successfully.`);
        }
      });
    }, 5000);
  }
}

// -----------part2-----------

// creating a JSON
function createJSONUsingPromise(details) {
  return new Promise((res, rej) => {
    fs.mkdir('./test/random1', (err) => {
      if (err) {
        console.log('Error', err);
      } else {
        console.log('Directory created successfully.');
        for (let name = 1; name < 4; name++) {
          const fileName = `./test/random1/file${name}.json`;
          fs.writeFile(fileName, details, (err) => {
            if (err) {
              console.log(`Error Creating file${name}`, err);
              rej(err);
            } else {
              console.log(`file${name} created successfully!`);
            }
          });
        }
        setTimeout(() => {
          for (let name = 1; name < 4; name++) {
            const deleteFileName = `./test/random1/file${name}.json`;
            fs.unlink(deleteFileName, (err) => {
              if (err) {
                console.log('Error', err);
                rej(err);
              } else {
                console.log(`file${name} deleted successfully`);
              }
            });
          }
          res('All files deleted successfully.');
        }, 7000);
      }
    });
  });
}

//-----------part3---------------

// Creating a JSON
async function createJSONUsingAsync(details) {
  try {
    await fs.mkdir(`./test/random2`, (err) => {
      if (!err) {
        console.log('Directory created successfully!');
      } else {
        console.log('Error', err);
      }
    });
    for (let name = 1; name < 4; name++) {
      const fileName = `./test/random2/${name}.json`;
      await fs.writeFile(fileName, details, (err) => {
        if (!err) {
          console.log(`file${name} created successfully.`);
        } else {
          console.log('Error', err);
        }
      });
    }

    //deleteing a JSON
    for (let name = 1; name < 4; name++) {
      const deleteFileName = `./test/random2/${name}.json`;
      await setTimeout(() => {
        fs.unlink(deleteFileName, (err) => {
          if (!err) {
            console.log(`file${name} deleted successfully.`);
          } else {
            console.log('Error', err);
          }
        });
      }, 10000);
    }
  } catch (err) {
    console.error('Error:', err);
  }
}

module.exports = {
  createJSONUsingFS,
  createJSONUsingPromise,
  createJSONUsingAsync,
};
