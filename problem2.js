const { unlink } = require('fs');

const fs = require('fs').promises;

async function readLipsum() {
  try {
    const readData = await fs.readFile('./lipsum.txt', 'utf8');
    return readData;
  } catch (err) {
    console.log('Error', err);
  }
}

async function makeUpperCase() {
  try {
    const data = await readLipsum();
    await fs.writeFile('./uppercase.txt', data.toUpperCase());

    await fs.appendFile('./filenames.txt', 'uppercase.txt\n');
    console.log('uppercase.txt created successfully');

    return 'uppercase.txt';
  } catch (err) {
    console.log(err);
  }
}

async function readUpperMakeLower() {
  try {
    const upperCaseData = await fs.readFile('./uppercase.txt', 'utf8');
    const lowerData = upperCaseData.toLowerCase().split('.');
    return lowerData;
  } catch (err) {
    console.log(err);
  }
}

async function createSentencesInFiles() {
  try {
    const data = await readUpperMakeLower();

    for (let i = 0; i < data.length; i++) {
      await fs.writeFile(`sentence${i}.txt`, data[i]);

      await fs.appendFile('./filenames.txt', `sentence${i}.txt\n`);
      console.log(`sentence${i}.txt created successfully`);
    }
  } catch (err) {
    console.log(err);
  }
}

async function readNewfilesAndSort() {
  try {
    const fileName = await fs.readFile('./filenames.txt', 'utf8');
    const array = fileName.split('\n');

    let mergedContent = '';
    for (let i = 0; i < array.length; i++) {
      const filePath = `./sentence${i}.txt`;
      try {
        await fs.access(filePath);
        const content = await fs.readFile(filePath, 'utf8');
        mergedContent += content;
      } catch (err) {
        console.log(`Error reading file ${filePath}: ${err.message}`);
      }
    }
    const sortedContent = mergedContent.split('\n').sort();
    await fs.writeFile('./sorted.txt', sortedContent);

    console.log('Sorted and saved content to sorted.txt');
    await fs.appendFile('./filenames.txt', 'sorted.txt \n');

    console.log('All files read, sorted and saved to sorted.txt');
  } catch (err) {
    console.log(err);
  }
}

async function deleteAllFiles() {
  try {
    const fileName = await fs.readFile('./filenames.txt', 'utf-8');
    const array = fileName.split('\n');
    console.log(array);

    for (let i = 0; i < array.length; i++) {
      if (array[i]) {
        // trim for removing unwanted spaces
        await unlink(array[i].trim(), (err) => {
          if (!err) {
            console.log('Success!');
          } else {
            console.log('Error', err);
          }
        });
      }
    }
  } catch (err) {
    console.log(err);
  }
}

// readNewfilesAndSort();

module.exports = {
  readLipsum,
  makeUpperCase,
  readUpperMakeLower,
  createSentencesInFiles,
  readNewfilesAndSort,
  deleteAllFiles,
};
// deleteAllFiles();
// readNewfilesAndSort();
// readNewfilesAndSort();
// makeUpperCase();
